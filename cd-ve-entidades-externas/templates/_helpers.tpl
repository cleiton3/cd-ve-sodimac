{{- /* Nombres de los componentes generales */ -}}
{{- define "gateway.name" }}
{{- printf "%s-%s" .Chart.Name "gwy"  | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}

{{- define "service.name" }}
{{- printf "%s-%s" .Chart.Name "svc"  | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}

{{- define "virtualservice.name" }}
{{- printf "%s-%s" .Chart.Name "vsvc" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}

{{- define "serviceaccount.name" }}
{{- printf "%s-%s" .Chart.Name "svca"  | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}

{{- define "destinationrule.name" }}
{{- printf "%s-%s" .Chart.Name "destrule"  | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}

{{- /* Nombres de los componentes */ -}}

{{- /* APP Version 1 */ -}}
{{- define "deployment.v1.name" }}
{{- printf "%s-%s-%s" .Chart.Name .Values.version1.version "deploy" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}
{{- define "hpa.v1.name" }}
{{- printf "%s-%s-%s" .Chart.Name .Values.version1.version "hpa" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}
{{- define "image.v1.name" }}
{{- printf "%s:%s" .Values.version1.repository .Values.version1.tag | replace "+" "_" | trimSuffix ":" }}
{{ end -}}