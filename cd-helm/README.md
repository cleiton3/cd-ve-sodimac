# Template de Despliegue Microservicios utilizando Helm

Este Chart de helm debe servir para el despliegue de los
microservicios del proyecto Venta Empresa Digital

Se tienen dos pipelines asociados a cada uno de los módulos de la solución Venta Empresa

1. Pipeline CI: Encargado de el Build del artefacto: 

    - Compilación de microservicio Java hacia Nativo. 
    - Construcción de Imagen Container (OCI complaint) 
    - Pull de Imagen en repositorio de imágenes
    - Para solución FrontEnd es una construcción de proyecto Angular 

2. Pipeline CD: Encargado de cargar los componentes de ejecución en OpenShift

    - Ejecución de Helm que define en el Chart los componentes Kubernetes
    - Para esta ejecución se utilizarán propiedades independientes por cada microservicio


    Desarrollo realizado por VCSOFT para Sodimac Colombia. Derechos reservados 2020.
     _   _______________  __________
    | | / / ___/ __/ __ \/ __/_  __/
    | |/ / /___\ \/ /_/ / _/  / /   
    |___/\___/___/\____/_/   /_/    vc-soft.com 2020
    

