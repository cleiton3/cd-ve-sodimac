{{- /* Nombres de los componentes generales */ -}}
{{- define "gateway.name" }}
{{- printf "%s-%s" .Chart.Name "gwy"  | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}

{{- define "service.name" }}
{{- printf "%s-%s" .Chart.Name "svc"  | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}

{{- define "virtualservice.name" }}
{{- printf "%s-%s" .Chart.Name "vsvc" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}

{{- define "serviceaccount.name" }}
{{- printf "%s-%s" .Chart.Name "svca"  | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}

{{- define "destinationrule.name" }}
{{- printf "%s-%s" .Chart.Name "destrule"  | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}

{{- /* Nombres de los componentes por version */ -}}

{{- /* APP values 1 */ -}}
{{- define "deployment.v1.name" }}
{{- printf "%s-%s-%s" .Chart.Name .Values.values1.version "deploy" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}
{{- define "hpa.v1.name" }}
{{- printf "%s-%s-%s" .Chart.Name .Values.values1.version "hpa" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}
{{- define "image.v1.name" }}
{{- printf "%s:%s" .Values.values1.repository .Values.values1.tag | replace "+" "_" | trimSuffix ":" }}
{{ end -}}
{{- define "configmap.v1.name" }}
{{- printf "%s-%s" .Values.values1.configmap.name "configmap" | replace "+" "_" | trimSuffix "-" }}
{{ end -}}

{{- /* APP values 2 */ -}}
{{- define "deployment.v2.name" }}
{{- printf "%s-%s-%s" .Chart.Name .Values.values2.version "deploy" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}
{{- define "hpa.v2.name" }}
{{- printf "%s-%s-%s" .Chart.Name .Values.values2.version "hpa" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}
{{- define "image.v2.name" }}
{{- printf "%s:%s" .Values.values2.repository .Values.values2.tag | replace "+" "_" | trimSuffix ":" }}
{{ end -}}
{{- define "configmap.v2.name" }}
{{- printf "%s-%s" .Values.values2.configmap.name "configmap" | replace "+" "_" | trimSuffix "-" }}
{{ end -}}

{{- /* APP values 3 */ -}}
{{- define "deployment.v3.name" }}
{{- printf "%s-%s-%s" .Chart.Name .Values.values3.version "deploy" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}
{{- define "hpa.v3.name" }}
{{- printf "%s-%s-%s" .Chart.Name .Values.values3.version "hpa" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{ end -}}
{{- define "image.v3.name" }}
{{- printf "%s:%s" .Values.values3.repository .Values.values3.tag | replace "+" "_" | trimSuffix ":" }}
{{ end -}}
{{- define "configmap.v3.name" }}
{{- printf "%s-%s" .Values.values3.configmap.name "configmap" | replace "+" "_" | trimSuffix "-" }}
{{ end -}}




